/**
 *      @author: Saray Pérez
 *      Aplicación que se conecta a un servidor ftp y baja un fichero de éste.
 */

package servidor;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import javax.swing.*;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BajarFTP {

    private static String ip = "192.168.0.160";
    private static Integer puerto = 21;
    private static String usuario = "formacion";
    private static String pass = "formacion";
    private static String fichero_local = "/Users/Sarai/Documents/PSP/tareas/bajarftp/src/main/java/servidor/archivo.txt";
    private static String fichero_servidor = "archivo.txt";
    private static FTPClient ftp;


    public static void main(String[] args) throws IOException {

        conectar();
        desrcargarArchivoFTP();
        desconectar();
    }

    public static void conectar() throws IOException {

        try {

            ftp = new FTPClient();
            ftp.connect(ip, puerto);
            ftp.login(usuario, pass);

            int respuesta = ftp.getReplyCode();
            if (FTPReply.isPositiveCompletion(respuesta)) {
                System.out.println("Conectado correctamente al servidor");
                JOptionPane.showMessageDialog(null, "Conectado correctamente al servidor");
            }
            else{
                System.out.println("Imposible conectar al servidor");
                JOptionPane.showMessageDialog(null, "Imposible conectar al servidor");
            }

        }catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Error");
        }

    }

    public static void desrcargarArchivoFTP() throws IOException {

        ftp.changeWorkingDirectory("/home/formacion");
        BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(fichero_local));

        if (ftp.retrieveFile(fichero_servidor, buffOut)) {
            System.out.println("El fichero " + fichero_servidor + " ha sido descargado con éxito");
            JOptionPane.showMessageDialog(null, "El fichero " + fichero_servidor + " ha sido descargado con éxito");
        }
        else {
            System.out.println("Error en la descarga del fichero");
            JOptionPane.showMessageDialog(null, "Error en la descarga del fichero");
        }

        buffOut.close();

    }

    public static void desconectar() {

        try {

            // Cerrar sesión
            ftp.logout();

            // Desconectarse al servidor
            ftp.disconnect();


        } catch (IOException e) {
            e.printStackTrace();

        }
    }

}